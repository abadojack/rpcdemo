struct intPair {
	int a;
	int b;
};

struct intTripple {
	int a;
	int b;
	int c;
};

struct intWrapper{
	int a;
};

program ARITH_PROG {
	version ARITH_VERS {
		int ADD(intPair)=1;
		int MUL(intTripple)=2;
		int SQR(intWrapper)=3;
	}=1;
}=0x3141;
