#include "arithmetic.h"

int *add_1_svc(intPair *argp, struct svc_req *rqstp)
{
	static int result;

	printf("Parameters: %d, %d\n", argp->a, argp->b);
	result = argp->a + argp->b;
	printf("Returning %d\n", result);

	return &result;
}

int *mul_1_svc(intTripple *argp, struct svc_req *rqstp)
{
	static int result;

	printf("Parameters: %d, %d and %d\n", argp->a, argp->b, argp->c);
	result = argp->a * argp->b * argp->c;
	printf("Returning %d\n", result);

	return &result;
}

int *sqr_1_svc(intWrapper *argp, struct svc_req *rqstp)
{
	static int result;
	
	printf("Parameter: %d\n", argp->a);
	result = argp->a * argp->a;
	printf("Returning %d\n", result);

	return &result;
}
