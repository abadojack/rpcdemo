.SUFFIXES:
.SUFFIXES: .c .o
CLNT = arithmetic_client
SRVR = arithmetic_svc
CFLAGS = -g -Wall

SRVR_OBJ = arithmetic_server.o arithmetic_xdr.o arithmetic_svc.o
CLNT_OBJ = arithmetic_client.o arithmetic_xdr.o arithmetic_clnt.o

.c.o:; gcc -c -o $@ $(CFLAGS) $<

default: $(CLNT) $(SRVR)

$(CLNT): $(CLNT_OBJ) arithmetic.h
	gcc -o $(CLNT) $(CLNT_OBJ)

$(SRVR): $(SRVR_OBJ) arithmetic.h
	gcc -o $(SRVR) $(SRVR_OBJ)

clean:
	rm *.o $(CLNT) $(SRVR)
