#include "arithmetic.h"

CLIENT *rpc_setup(char *host);
void add(CLIENT *clnt, int, int);
void mul(CLIENT *clnt, int, int, int);
void sqr(CLIENT * clnt, int);

int main(int argc, char *argv[])
{
	CLIENT *clnt; //client handle to server
	char *host; //host
	int a, b, c, option;

	if(argc != 2){
		printf("Usage: %s server_host\n", argv[0]);
		return 0;
	}

	host = argv[1];
	if((clnt = rpc_setup(host)) == 0){
			return 1;
	}

	printf("Options\n1.Add\n2.Multiply\n3.Square\n");
	scanf("%d", &option);

	switch(option) {
		case 1:
			printf("Enter the two integers to add\n");
			scanf("%d%d", &a, &b);

			add(clnt,a,b);
		break;

		case 2:
		printf("Enter 3 integers to multiply\n");
		scanf("%d%d%d", &a,&b,&c);

		mul(clnt, a, b, c);
		break;

		case 3:
			printf("Enter the number to square\n");
			scanf("%d", &a);
			
			sqr(clnt, a);

		break;

	}

	clnt_destroy(clnt);

	return 0;
}


CLIENT *rpc_setup(char *host)
{
	CLIENT *clnt = clnt_create(host, ARITH_PROG, ARITH_VERS, "udp");
	if (clnt == NULL) {
		clnt_pcreateerror(host);
		return 0;
	}
	return clnt;
}

void add(CLIENT *clnt, int a, int b)
{
	int  *result;
	intPair v;	/* parameter for add */

	v.a = a;
	v.b = b;
	result = add_1(&v, clnt);
	if (result == 0) {
		clnt_perror(clnt, "call failed");
	} else {
		printf("%d\n", *result);
	}
}


void mul(CLIENT *clnt, int a, int b, int c)
{
	int  *result;
	intTripple v;

	v.a = a;
	v.b = b;
	v.c = c;
	result = mul_1(&v, clnt);
	if (result == 0) {
		clnt_perror(clnt, "call failed");
	} else {
		printf("%d\n", *result);
	}
}


void sqr(CLIENT *clnt, int a)
{
	int  *result;
	intWrapper v;	/* parameter for sqr */

	v.a = a;

	result = sqr_1(&v, clnt);
	if (result == 0) {
		clnt_perror(clnt, "call failed");
	} else {
		printf("%d\n", *result);
	}
}
